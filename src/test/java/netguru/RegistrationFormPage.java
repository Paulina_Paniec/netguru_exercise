package netguru;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationFormPage extends PageObject {

    @FindBy(name = "firstName")
    public WebElement firstNameInput;

    @FindBy(name = "lastName")
    public WebElement lastNameInput;

    @FindBy(id = "userName")
    public WebElement emailInput;

    @FindBy(id = "email")
    public WebElement usernameInput;

    @FindBy(name = "password")
    public WebElement passwordInput;

    @FindBy(name = "confirmPassword")
    public WebElement confirmPasswordInput;

    @FindBy(name = "register")
    public WebElement submitButton;

    @FindBy(xpath = "//font[contains(., 'Thank you for registering.')]")
    public WebElement succesfulRegistrationMessage;

    public RegistrationFormPage(WebDriver driver) {
        super(driver);
    }

    public void provideFirstName(String firstName) {
        waitUntilElementIsClickable(firstNameInput);
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
    }

    public void provideLastName(String lastName) {
        waitUntilElementIsClickable(lastNameInput);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
    }

    public void provideEmail(String email) {
        waitUntilElementIsClickable(emailInput);
        emailInput.clear();
        emailInput.sendKeys(email);
    }

    public void provideUsername(String username) {
        waitUntilElementIsClickable(usernameInput);
        usernameInput.clear();
        usernameInput.sendKeys(username);
    }

    public void providePassword(String password) {
        waitUntilElementIsClickable(passwordInput);
        passwordInput.clear();
        passwordInput.sendKeys(password);
    }

    public void confirmPassword(String password) {
        waitUntilElementIsClickable(confirmPasswordInput);
        confirmPasswordInput.clear();
        confirmPasswordInput.sendKeys(password);
    }

    public void clickSubmitButton() {
        waitUntilElementIsClickable(submitButton);
        submitButton.click();
    }

    public void assertRegistrationWasSuccesful() {
        try {
            waitUntilVisibilityOfElement(succesfulRegistrationMessage);
        } catch (TimeoutException e) {
            throw new RuntimeException("succesful registration message didn't appear");
        }
    }
}
