package netguru;

import lombok.Data;

@Data
public class User {
    String firstName;
    String lastName;
    String email;
    String username;
    String password;
}
