package netguru;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;


@Slf4j
public class PageObject {

    public static final int TIMEOUT_IN_SECONDS = 30;
    public static final int POOLING_TIME_IN_MILLIS = 100;

    @FindBy(linkText = "REGISTER")
    public WebElement registerToolbarButton;

    @FindBy(linkText = "SIGN-ON")
    public WebElement signOnToolbarButton;

    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void waitUntilVisibilityOfElement(WebElement element) {
        waitUntilConditionMeet(ExpectedConditions.visibilityOf(element));
    }

    public <T> T waitUntilConditionMeet(ExpectedCondition<T> condition) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(TIMEOUT_IN_SECONDS))
                .pollingEvery(Duration.ofMillis(POOLING_TIME_IN_MILLIS))
                .ignoring(NoSuchElementException.class)
                .until(condition);
    }

    public void waitUntilElementIsClickable(WebElement element) {
        waitUntilConditionMeet(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntilElementIsStale(WebElement element) {
        waitUntilConditionMeet(ExpectedConditions.stalenessOf(element));
    }

    public void getAppsMainPage() {
        driver.get(Properties.MAIN_APP_URL);
    }

    public void clickRegisterToolbarButton() {
        waitUntilElementIsClickable(registerToolbarButton);
        registerToolbarButton.click();
    }

    public void clickSignUpToolbarButton() {
        waitUntilElementIsClickable(signOnToolbarButton);
        signOnToolbarButton.click();
    }
}