package netguru;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpPage extends PageObject {

    @FindBy(name = "userName")
    public WebElement usernameInput;

    @FindBy(name = "password")
    public WebElement passwordInput;

    @FindBy(name = "login")
    public WebElement submitButton;


    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    public void provideUsername(String username) {
        waitUntilElementIsClickable(usernameInput);
        usernameInput.clear();
        usernameInput.sendKeys(username);
    }

    public void clearPasswordInput() {
        waitUntilElementIsClickable(passwordInput);
        passwordInput.clear();
    }

    public void clickSubmitButton() {
        waitUntilElementIsClickable(submitButton);
        submitButton.click();
    }
}