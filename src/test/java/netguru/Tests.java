package netguru;

import com.github.javafaker.Faker;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Tests {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void registrationTest() {

        User user = new User();
        Faker faker = new Faker();
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setEmail(faker.internet().emailAddress());
        user.setUsername(faker.name().username());
        user.setPassword(faker.animal().name());

        PageObject pageObject = new PageObject(driver);
        pageObject.getAppsMainPage();
        pageObject.clickRegisterToolbarButton();

        RegistrationFormPage page = new RegistrationFormPage(driver);
        page.provideFirstName(user.getFirstName());
        page.provideLastName(user.getLastName());
        page.provideEmail(user.getEmail());
        page.provideUsername(user.getUsername());
        page.providePassword(user.getPassword());
        page.confirmPassword(user.getPassword());
        page.clickSubmitButton();
        page.assertRegistrationWasSuccesful();
    }

    @Test
    public void whenLoginWithoutPasswordThenUserNotLoggedAndCredentialsCleared() {

        PageObject pageObject = new PageObject(driver);
        pageObject.getAppsMainPage();
        pageObject.clickSignUpToolbarButton();

        SignUpPage signUpPage = new SignUpPage(driver);
        pageObject.waitUntilVisibilityOfElement(signUpPage.usernameInput);
        WebElement usernameInputToBecomeStale = driver.findElement(By.name("userName"));
        signUpPage.provideUsername(new Faker().name().username());
        signUpPage.clearPasswordInput();
        signUpPage.clickSubmitButton();
        pageObject.waitUntilElementIsStale(usernameInputToBecomeStale);

        String emptyString = "";
        assertEquals(emptyString, signUpPage.usernameInput.getText(),
                "username input expected to be empty, but was: " + signUpPage.usernameInput.getText());
        assertEquals(emptyString, signUpPage.passwordInput.getText(),
                "password input expected to be empty, but was: " + signUpPage.passwordInput.getText());
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }
}
